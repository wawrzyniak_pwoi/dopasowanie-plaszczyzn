from scipy.stats import norm
from csv import writer, reader
from math import pi, sin, cos, sqrt
import numpy as np
from sklearn.cluster import KMeans
import random
import matplotlib.pyplot as plt
import pyransac3d


#  generowanie punktów w cylindrze
def generate_points_cylindrical(num_points: int = 5000, x_center: int = 400, y_center: int = 400, radius: int = 10):
    x = []
    y = []
    z = []
    for i in range(num_points):
        area = radius**2 * pi
        theta = random.uniform(0, 2*pi)
        length = sqrt(random.uniform(0, area/pi))
        x.append(x_center + length * cos(theta))
        y.append(y_center + length * sin(theta))
        z.append(random.randrange(0, 200))

    points = zip(x, y, z)
    return points


#  generowanie punktów na płaszczyźnie poziomej Y
def generate_points_horizontal(num_points: int = 2000, x_loc=-300, y_loc=-300):
    distribution_x = norm(loc=x_loc, scale=20)
    distribution_y = norm(loc=y_loc, scale=200)
    distribution_z = norm(loc=0.2, scale=0.05)

    x = distribution_x.rvs(size=num_points)
    y = distribution_y.rvs(size=num_points)
    z = distribution_z.rvs(size=num_points)

    points = zip(x, y, z)
    return points


#  generowanie punktów na płaszczyźnie pionowej Z
def generate_points_vertical(num_points: int = 2000, x_loc=0, z_loc=0):
    distribution_x = norm(loc=x_loc, scale=20)
    distribution_y = norm(loc=0, scale=0.05)
    distribution_z = norm(loc=z_loc, scale=200)

    x = distribution_x.rvs(size=num_points)
    y = distribution_y.rvs(size=num_points)
    z = distribution_z.rvs(size=num_points)

    points = zip(x, y, z)
    return points


if __name__ == '__main__':
    cloud_points = generate_points_cylindrical(5000, -300, -300, 20)
    with open('Punkty.xyz', 'w', encoding='utf-8', newline='\n') as csvfile:
        csvwriter = writer(csvfile)
        for p in cloud_points:
            csvwriter.writerow(p)

    cloud_points = generate_points_horizontal(2000, 0, 0)
    with open('Punkty.xyz', 'a', encoding='utf-8', newline='\n') as csvfile:
        csvwriter = writer(csvfile)
        for p in cloud_points:
            csvwriter.writerow(p)

    cloud_points = generate_points_vertical(3000, 500, 200)
    with open('Punkty.xyz', 'a', encoding='utf-8', newline='\n') as csvfile:
        csvwriter = writer(csvfile)
        for p in cloud_points:
            csvwriter.writerow(p)

    re_read = []
    with open("Punkty.xyz", newline='') as csvfile:
        r = reader(csvfile, delimiter=',')
        for line in r:
            re_read.append([float(line[0]), float(line[1]), float(line[2])])
        print(re_read)

    clusterer = KMeans(n_clusters=3)
    X = np.array(re_read)
    pred = clusterer.fit_predict(X)
    red = pred == 0
    blue = pred == 1
    green = pred == 2
    plt.figure()
    plt.scatter(X[red, 0], X[red, 1], c="r")
    plt.scatter(X[blue, 0], X[blue, 1], c="b")
    plt.scatter(X[green, 0], X[green, 1], c="g")
    plt.show()

    #   Ransac
    cloud = np.array(re_read)
    plane = pyransac3d.Plane()
    best_eq, best_inliers = plane.fit(cloud, thresh=0.01, minPoints=100, maxIteration=1000)

    print(f'best equation Ax+By+Cz+D:{best_eq}')
    print(f'best inliers:{best_inliers}')
